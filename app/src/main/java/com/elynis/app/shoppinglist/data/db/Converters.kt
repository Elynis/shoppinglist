package com.elynis.app.shoppinglist.data.db

import androidx.room.TypeConverter
import java.util.*

class Converters {
    @TypeConverter
    fun toShoppingItemUnit(value: String) = enumValueOf<ShoppingItemUnit>(value)

    @TypeConverter
    fun fromShoppingItemUnit(value: ShoppingItemUnit) = value.name
}