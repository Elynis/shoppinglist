package com.elynis.app.shoppinglist.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArchivedListsViewModel @Inject constructor(
    val repository: ShoppingRepository
) : ViewModel() {

    val archivedListsSortedByDate = repository.getAllArchivedShoppingListsSortedByDate()

    fun insertShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertShoppingList(shoppingList)
    }

    fun deleteShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteShoppingList(shoppingList)
    }
}