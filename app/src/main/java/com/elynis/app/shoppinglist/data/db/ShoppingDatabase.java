package com.elynis.app.shoppinglist.data.db;


import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem;
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList;

@Database(
        version = 2,
        entities = {
                ShoppingItem.class,
                ShoppingList.class
        }
)
@TypeConverters(Converters.class)
abstract public class ShoppingDatabase extends RoomDatabase {
    abstract public ShoppingDao getShoppingDao();
}



