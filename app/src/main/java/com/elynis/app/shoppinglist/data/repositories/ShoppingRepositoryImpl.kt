package com.elynis.app.shoppinglist.data.repositories

import com.elynis.app.shoppinglist.data.db.ShoppingDao
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import javax.inject.Inject

class ShoppingRepositoryImpl @Inject constructor(
    private val shoppingDao: ShoppingDao
) : ShoppingRepository{
    override suspend fun dropShoppingListsTable() = shoppingDao.dropShoppingListsTable()
    override suspend fun dropShoppingItemsTable() = shoppingDao.dropShoppingItemsTable()

    override suspend fun insertShoppingItem(item: ShoppingItem) = shoppingDao.insertShoppingItem(item)
    override suspend fun deleteShoppingItem(item: ShoppingItem) = shoppingDao.deleteShoppingItem(item)

    override suspend fun insertShoppingList(list: ShoppingList) : Long = shoppingDao.insertShoppingList(list)
    override suspend fun deleteShoppingList(list: ShoppingList) = shoppingDao.deleteShoppingList(list)

    override fun getAllShoppingItems() = shoppingDao.getAllShoppingItems()
    override fun getAllShoppingItemsSortedByName() = shoppingDao.getAllShoppingItemsSortedByName()
    override fun getShoppingItemsOfShoppingList(id: Int) = shoppingDao.getShoppingItemsOfShoppingList(id)

    override fun getAllShoppingLists() = shoppingDao.getAllShoppingLists()
    override fun getAllShoppingListsSortedByDate() = shoppingDao.getAllShoppingListsSortedByDate()
    override fun getAllActiveShoppingListsSortedByDate() = shoppingDao.getAllActiveShoppingListsSortedByDate()
    override fun getAllArchivedShoppingListsSortedByDate() = shoppingDao.getAllArchivedShoppingListsSortedByDate()

}
