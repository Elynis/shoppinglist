package com.elynis.app.shoppinglist.ui.dialog

import android.R
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import com.elynis.app.shoppinglist.R.layout.dialog_add_shopping_item
import com.elynis.app.shoppinglist.data.db.ShoppingItemUnit
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.util.roundToDecimals
import kotlinx.android.synthetic.main.dialog_add_shopping_item.*
import kotlinx.android.synthetic.main.fragment_list_details.btnAdd
import timber.log.Timber

class AddOrEditShoppingItemDialog(
    context: Context,
    var addOrEditDialogListener: AddOrEditDialogListener,
    var shoppingItem: ShoppingItem? = null
) :
    AppCompatDialog(context) {

    private val units = ShoppingItemUnit.values()
        .filter { it.shouldBeDisplayed }
        .map { context.getString(it.description) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(dialog_add_shopping_item)
        populateView(shoppingItem)
        btnAdd.setOnClickListener { onBtnAddClicked() }
        btnCancel.setOnClickListener { cancel() }
    }

    private fun onBtnAddClicked() {
        try {
            val name = etItemName.text.toString()
            val quantityString = etQuantity.text.toString()
            val priceString = etPrice.text.toString().replace(",", ".")
            val unit = ShoppingItemUnit.descriptionOf(context, spnUnits.selectedItem as String)

            if (name.isEmpty() || quantityString.isEmpty() || priceString.isEmpty()) {
                Toast.makeText(context, context.getString(com.elynis.app.shoppinglist.R.string.invalid_add_or_edit_dialog_form_empty), Toast.LENGTH_SHORT).show()
            }

            val quantity = quantityString.toInt()
            val price = priceString.toFloat()

            val item = ShoppingItem(
                id = shoppingItem?.id,
                name = name,
                quantity = quantity,
                cost = price,
                unit = unit,
                isCompleted = shoppingItem?.isCompleted ?: false
            )
            addOrEditDialogListener.onAddBtnClicked(item)
            dismiss()

        } catch (e: Exception) {
            Timber.e(
                listOf(
                    etItemName.text.toString(),
                    etQuantity.text.toString(),
                    etPrice.text.toString(),
                    spnUnits.selectedItem
                ).toString()
            )
            Toast.makeText(context, context.getString(com.elynis.app.shoppinglist.R.string.invalid_add_or_edit_dialog_type_conversion), Toast.LENGTH_SHORT).show()
        }
    }

    private fun populateView(shoppingItem: ShoppingItem?) {
        ArrayAdapter(context, R.layout.simple_spinner_item, units).apply {
            setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            spnUnits.adapter = this
        }

        shoppingItem?.also { it ->
            etItemName.setText(it.name)
            etQuantity.setText(it.quantity.toString())
            etPrice.setText(it.cost.roundToDecimals(2).toString())
            spnUnits.setSelection(units.indexOf(context.getString(it.unit.description)))
            btnAdd.text = context.getString(com.elynis.app.shoppinglist.R.string.add_or_edit_dialog_on_edit_text)

        } ?: run {
            etItemName.setText("")
            etQuantity.setText("")
            etPrice.setText("")
            spnUnits.setSelection(units.indexOf(context.getString(ShoppingItemUnit.DEFAULT.description)))
            btnAdd.text = context.getString(com.elynis.app.shoppinglist.R.string.add_or_edit_dialog_on_add_text)
        }
    }

}