package com.elynis.app.shoppinglist.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.elynis.app.shoppinglist.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView.setupWithNavController(navHostFragment.findNavController())
        setupNavHostDestinationChangedListener()
    }

    private fun setupNavHostDestinationChangedListener() {
        navHostFragment.findNavController()
            .addOnDestinationChangedListener { _, destination, _ ->
                supportActionBar?.title = when (destination.id) {
                    R.id.activeListFragment -> getString(R.string.active_lists_title)
                    R.id.archivedListFragment -> getString(R.string.archived_lists_title)
                    R.id.listDetailsFragment -> getString(R.string.list_details_title)
                    else -> getString(R.string.unknown_title)
                }

            }
    }
}