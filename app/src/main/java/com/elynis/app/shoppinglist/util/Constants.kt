package com.elynis.app.shoppinglist.util

object Constants {
    const val SHOPPING_DATABASE_NAME = "shopping_db"
    const val SHOPPING_LIST_ID_ARGUMENT_NAME = "shoppingListId"
}