package com.elynis.app.shoppinglist.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ListDetailsViewModel @Inject constructor(
    val repository: ShoppingRepository
) : ViewModel() {

    private val _shoppingListWithShoppingItems = MediatorLiveData<ShoppingListWithShoppingItems>()
    val shoppingListWithShoppingItems: LiveData<ShoppingListWithShoppingItems>
        get() = _shoppingListWithShoppingItems

    fun setupLiveDataOfShoppingList(id: Int?) {
        if (id != null) {
            _shoppingListWithShoppingItems.addSource(repository.getShoppingItemsOfShoppingList(id)) {
                _shoppingListWithShoppingItems.value = it
            }
        } else {
            insertShoppingList(
                ShoppingList(
                    name = "List Name",
                    timestampOfCreation = System.currentTimeMillis(),
                    timestampOfLastChange = System.currentTimeMillis()
                )
            )
        }
    }

    fun insertShoppingItem(shoppingItem: ShoppingItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertShoppingItem(shoppingItem)
    }

    fun deleteShoppingItem(shoppingItem: ShoppingItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteShoppingItem(shoppingItem)
    }

    private fun insertShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.IO) {
        val newId = repository.insertShoppingList(shoppingList).toInt()
        withContext(Dispatchers.Main) {
            _shoppingListWithShoppingItems.addSource(repository.getShoppingItemsOfShoppingList(newId)) {
                _shoppingListWithShoppingItems.value = it
            }
        }
    }
}