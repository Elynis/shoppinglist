package com.elynis.app.shoppinglist.di

import android.content.Context
import androidx.room.Room
import com.elynis.app.shoppinglist.adapters.ItemsAdapter
import com.elynis.app.shoppinglist.data.db.ShoppingDao
import com.elynis.app.shoppinglist.data.db.ShoppingDatabase
import com.elynis.app.shoppinglist.data.db.migrations.Migrations.MIGRATION_1_2
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepositoryImpl
import com.elynis.app.shoppinglist.ui.viewmodels.ListDetailsViewModel
import com.elynis.app.shoppinglist.util.Constants.SHOPPING_DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Provides
    @Singleton
    fun provideShoppingDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        ShoppingDatabase::class.java,
        SHOPPING_DATABASE_NAME
    ).addMigrations(MIGRATION_1_2).build()


    @Singleton
    @Provides
    fun provideShoppingDAO(db: ShoppingDatabase): ShoppingDao = db.shoppingDao


    @Singleton
    @Provides
    fun provideShoppingRepository(dao: ShoppingDao): ShoppingRepository = ShoppingRepositoryImpl(dao)

}