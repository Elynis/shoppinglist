package com.elynis.app.shoppinglist.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.adapters.ArchivedListsAdapter
import com.elynis.app.shoppinglist.ui.viewmodels.ArchivedListsViewModel
import com.elynis.app.shoppinglist.util.Constants
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_archived_lists.*

@AndroidEntryPoint
class ArchivedListsFragment: Fragment(R.layout.fragment_archived_lists) {

    private val viewModel: ArchivedListsViewModel by viewModels()

    private lateinit var archivedListsAdapter: ArchivedListsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        viewModel.archivedListsSortedByDate.observe(viewLifecycleOwner, {
            archivedListsAdapter.submitList(it)
        })
    }

    private fun navigateToListDetails(shoppingListId: String?) {
//        val bundle = bundleOf(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME to shoppingListId)
//        findNavController().navigate(R.id.action_activeListFragment_to_listDetailsFragment, bundle)
    }

    private fun setupRecyclerView() = rvArchivedLists.apply{
        archivedListsAdapter = ArchivedListsAdapter().apply {
            setOnItemClicked{
                navigateToListDetails(it.shoppingList.id.toString())
            }
            setOnUnarchiveBtnClicked {
                it.shoppingList.isArchived = false
                viewModel.insertShoppingList(it.shoppingList)
            }
        }

        adapter = archivedListsAdapter
        layoutManager = LinearLayoutManager(requireContext())
        ItemTouchHelper(itemTouchCallback).attachToRecyclerView(this)
    }

    private val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = true

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.layoutPosition
            val list = archivedListsAdapter.differ.currentList[position].shoppingList
            viewModel.deleteShoppingList(list)

            Snackbar.make(requireView(), getString(R.string.snackbar_item_sucessfully_deleted), Snackbar.LENGTH_LONG).apply {
                setAction(getString(R.string.undo)) {
                    viewModel.insertShoppingList(list)
                }
                show()
            }
        }
    }
}