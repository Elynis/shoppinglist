package com.elynis.app.shoppinglist.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems

@Dao
interface ShoppingDao {

    @Query("DELETE FROM shopping_list")
    suspend fun dropShoppingListsTable()

    @Query("DELETE FROM shopping_items")
    suspend fun dropShoppingItemsTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShoppingItem(item: ShoppingItem)

    @Delete
    suspend fun deleteShoppingItem(item: ShoppingItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShoppingList(list: ShoppingList): Long

    @Delete
    suspend fun deleteShoppingList(list: ShoppingList)

    @Query("SELECT * FROM shopping_items")
    fun getAllShoppingItems(): LiveData<List<ShoppingItem>>

    @Query("SELECT * FROM shopping_items ORDER BY name")
    fun getAllShoppingItemsSortedByName(): LiveData<List<ShoppingItem>>

    @Transaction
    @Query("SELECT * FROM shopping_list")
    fun getAllShoppingLists(): LiveData<List<ShoppingListWithShoppingItems>>

    @Transaction
    @Query("SELECT * FROM shopping_list ORDER BY timestampOfCreation DESC")
    fun getAllShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>

    @Transaction
    @Query("SELECT * FROM shopping_list WHERE isArchived = 0 ORDER BY timestampOfCreation DESC")
    fun getAllActiveShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>

    @Transaction
    @Query("SELECT * FROM shopping_list WHERE isArchived = 1 ORDER BY timestampOfCreation DESC")
    fun getAllArchivedShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>

    @Transaction
    @Query("SELECT * FROM shopping_list WHERE id = :id")
    fun getShoppingItemsOfShoppingList(id: Int): LiveData<ShoppingListWithShoppingItems>
}