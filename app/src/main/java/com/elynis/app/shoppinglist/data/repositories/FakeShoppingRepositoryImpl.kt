package com.elynis.app.shoppinglist.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems

class FakeShoppingRepositoryImpl : ShoppingRepository {

    private var itemsSequencer = 0
        get() {
            return field++
        }

    private var listSequencer = 0
        get() {
            return field++
        }


    private val shoppingItems = mutableListOf<ShoppingItem>()
    private val observableShoppingItems = MutableLiveData<List<ShoppingItem>>(shoppingItems)

    private val shoppingLists = mutableListOf<ShoppingList>()
    private val observableShoppingLists = MutableLiveData<List<ShoppingList>>(shoppingLists)

    private val shoppingListsWithShoppingItems = mutableListOf<ShoppingListWithShoppingItems>()
    private val observableShoppingListsWithShoppingItems =
        MutableLiveData<List<ShoppingListWithShoppingItems>>(shoppingListsWithShoppingItems)

    private fun refreshLiveData() {
        observableShoppingItems.postValue(shoppingItems)
        observableShoppingLists.postValue(shoppingLists)

        shoppingListsWithShoppingItems.removeAll { true }

        for (group in shoppingItems.groupBy { it.shoppingListId }){
            shoppingLists.find { it.id == group.key }?.let {
                shoppingListsWithShoppingItems.add(
                    ShoppingListWithShoppingItems(
                        it,
                        group.value
                    )
                )
            }
        }

        observableShoppingListsWithShoppingItems.postValue(shoppingListsWithShoppingItems)
    }

    override suspend fun dropShoppingListsTable() {
        shoppingLists.removeAll { true }
        shoppingListsWithShoppingItems.removeAll { true }
        refreshLiveData()
    }

    override suspend fun dropShoppingItemsTable() {
        shoppingItems.removeAll { true }
        refreshLiveData()
    }

    override suspend fun insertShoppingItem(item: ShoppingItem) {
        if (item.id == null) item.id = itemsSequencer
        shoppingItems.find { it.id == item.id }?.apply {
            name = item.name
            quantity = item.quantity
            cost = item.cost
            unit = item.unit
            shoppingListId = item.shoppingListId
            isCompleted = item.isCompleted
        } ?: run {
            shoppingItems.add(item)
        }
        refreshLiveData()
    }

    override suspend fun deleteShoppingItem(item: ShoppingItem) {
        shoppingItems.remove(item)
        refreshLiveData()
    }

    override suspend fun insertShoppingList(list: ShoppingList): Long {
        if (list.id == null) list.id = itemsSequencer
        shoppingLists.find { it.id == list.id }?.apply {
            name = list.name
            timestampOfCreation = list.timestampOfCreation
            timestampOfLastChange = list.timestampOfLastChange
            isArchived = list.isArchived
        } ?: run {
            shoppingLists.add(list)
        }
        refreshLiveData()
        return list.id?.toLong()!!
    }

    override suspend fun deleteShoppingList(list: ShoppingList) {
        shoppingLists.remove(list)
        refreshLiveData()
    }

    override fun getAllShoppingItems(): LiveData<List<ShoppingItem>> {
        return observableShoppingItems
    }

    override fun getAllShoppingItemsSortedByName(): LiveData<List<ShoppingItem>> {
        shoppingItems.sortBy { it.name }
        refreshLiveData()
        return observableShoppingItems
    }

    override fun getShoppingItemsOfShoppingList(id: Int): LiveData<ShoppingListWithShoppingItems> {
        shoppingLists.find { it.id == id }?.let {
            val items = shoppingItems.filter { it.shoppingListId == id }
            return MutableLiveData(
                ShoppingListWithShoppingItems(
                    it,
                    items
                )
            )
        }
        return MutableLiveData(null)
    }

    override fun getAllShoppingLists(): LiveData<List<ShoppingListWithShoppingItems>> {
        return observableShoppingListsWithShoppingItems
    }

    override fun getAllShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>> {
        shoppingListsWithShoppingItems.sortByDescending { it.shoppingList.timestampOfCreation }
        refreshLiveData()
        return observableShoppingListsWithShoppingItems
    }

    override fun getAllActiveShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>> {
        val sorted = shoppingListsWithShoppingItems.filter { !it.shoppingList.isArchived }.sortedByDescending { it.shoppingList.timestampOfCreation  }
        shoppingListsWithShoppingItems.removeAll{true}
        shoppingListsWithShoppingItems.addAll(sorted)
        refreshLiveData()
        return observableShoppingListsWithShoppingItems
    }

    override fun getAllArchivedShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>> {
        val sorted = shoppingListsWithShoppingItems.filter { it.shoppingList.isArchived }.sortedByDescending { it.shoppingList.timestampOfCreation  }
        shoppingListsWithShoppingItems.removeAll{true}
        shoppingListsWithShoppingItems.addAll(sorted)
        refreshLiveData()
        return observableShoppingListsWithShoppingItems
    }
}