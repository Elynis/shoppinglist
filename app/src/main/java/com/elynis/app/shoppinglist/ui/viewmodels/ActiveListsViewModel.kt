package com.elynis.app.shoppinglist.ui.viewmodels

import androidx.lifecycle.*
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepository
import com.elynis.app.shoppinglist.data.repositories.ShoppingRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ActiveListsViewModel @Inject constructor(
    val repository: ShoppingRepository
) : ViewModel() {

    val _currentLists = MediatorLiveData<List<ShoppingListWithShoppingItems>>()
    val currentLists: LiveData<List<ShoppingListWithShoppingItems>>
        get() = _currentLists

    private var cachedShoppingLists = listOf<ShoppingListWithShoppingItems>()
    private var isSearchStarting = true
    var isSearching = MutableLiveData(false)

    init {
        initCurrentList()
    }

    fun initCurrentList(){
        _currentLists.addSource(repository.getAllActiveShoppingListsSortedByDate()) {
            _currentLists.value = it
        }
    }

    fun insertShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertShoppingList(shoppingList)
    }

    fun deleteShoppingList(shoppingList: ShoppingList) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteShoppingList(shoppingList)
    }

    fun searchShoppingList(query: String) = viewModelScope.launch(Dispatchers.Default) {
        val listToSearch = if (isSearchStarting) {
            _currentLists.value
        } else {
            cachedShoppingLists
        }
        if (query.isEmpty()) {
            withContext(Dispatchers.Main) {
                _currentLists.value = cachedShoppingLists
                isSearching.value = false
            }
            isSearchStarting = true
            return@launch
        }
        val results = listToSearch?.filter {
            it.shoppingList.name.contains(query.trim(), ignoreCase = true)
        }
        if (isSearchStarting) {
            _currentLists.value?.let { cachedShoppingLists = it }
            isSearchStarting = false
        }
        results?.let {
            withContext(Dispatchers.Main) {
                _currentLists.value = it
            }
        }
        withContext(Dispatchers.Main) {
            isSearching.value = true
        }
    }
}