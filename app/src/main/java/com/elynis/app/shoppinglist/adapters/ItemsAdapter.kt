package com.elynis.app.shoppinglist.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.util.Currency
import kotlinx.android.synthetic.main.item_shopping_item.view.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ItemsAdapter @Inject constructor() : RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {

    private var onCheckedBtnClicked: ((Int,ShoppingItem) -> Unit)? = null
    fun setOnCheckedBtnClicked(listener: (Int, ShoppingItem) -> Unit) {
        onCheckedBtnClicked = listener
    }

    private var onEditBtnClicked: ((ShoppingItem) -> Unit)? = null
    fun setOnEditBtnClicked(listener: (ShoppingItem) -> Unit) {
        onEditBtnClicked = listener
    }

    inner class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    val diffCallback = object : DiffUtil.ItemCallback<ShoppingItem>(){
        override fun areItemsTheSame(oldItem: ShoppingItem, newItem: ShoppingItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ShoppingItem, newItem: ShoppingItem): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(list: List<ShoppingItem>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        return ItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_shopping_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        val item = differ.currentList[position]

        holder.itemView.apply {
            tvItemName.text = item.name
            val quantity = "${item.quantity} ${context.getString(item.unit.description)}"
            tvQuantity.text = quantity
            val price = "${item.cost} ${Currency.DOLAR.description}"
            tvPrice.text = price

            ibChecked.setOnClickListener {
                onCheckedBtnClicked?.let{
                    it(position, item)
                }
            }
            ibEdit.setOnClickListener {
                onEditBtnClicked?.let{
                    it(item)
                }
            }
            ibChecked.setImageResource( if (item.isCompleted) R.drawable.ic_baseline_check_box_32 else R.drawable.ic_round_check_box_outline_blank_32 )
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}