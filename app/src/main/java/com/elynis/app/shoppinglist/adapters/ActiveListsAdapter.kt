package com.elynis.app.shoppinglist.adapters

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.util.divideToPercent
import kotlinx.android.synthetic.main.circular_progress_indicator.view.*
import kotlinx.android.synthetic.main.item_active_shopping_list.view.*
import java.text.SimpleDateFormat
import java.util.*


class ActiveListsAdapter : RecyclerView.Adapter<ActiveListsAdapter.ActiveListsViewHolder>() {

    private var onItemClicked: ((ShoppingListWithShoppingItems) -> Unit)? = null
    fun setOnItemClicked(listener: (ShoppingListWithShoppingItems) -> Unit) {
        onItemClicked = listener
    }

    private var onArchiveBtnClicked: ((ShoppingListWithShoppingItems) -> Unit)? = null
    fun setOnArchiveBtnClicked(listener: (ShoppingListWithShoppingItems) -> Unit) {
        onArchiveBtnClicked = listener
    }

    inner class ActiveListsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val diffCallback = object : DiffUtil.ItemCallback<ShoppingListWithShoppingItems>() {
        override fun areItemsTheSame(
            oldItem: ShoppingListWithShoppingItems,
            newItem: ShoppingListWithShoppingItems
        ): Boolean {
            return oldItem.shoppingList.id == newItem.shoppingList.id
        }

        override fun areContentsTheSame(
            oldItem: ShoppingListWithShoppingItems,
            newItem: ShoppingListWithShoppingItems
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(list: List<ShoppingListWithShoppingItems>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveListsViewHolder {
        return ActiveListsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_active_shopping_list,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ActiveListsViewHolder, position: Int) {
        val listWithItems = differ.currentList[position]

        holder.itemView.apply {
            val completedItemsCount = listWithItems.shoppingItems.filter { it.isCompleted }.count()
            val itemsCount = listWithItems.shoppingItems.count()
            val progress = completedItemsCount.divideToPercent(itemsCount)

            "$completedItemsCount / $itemsCount".also { tvProgress.text = it }

            cpiProgress.progress = progress
            tvListName.text = listWithItems.shoppingList.name

            val creationCalendar = Calendar.getInstance().apply {
                timeInMillis = listWithItems.shoppingList.timestampOfCreation
            }

            val creationDateFormat = SimpleDateFormat("dd.MM.yy", Locale.getDefault())
            tvListCreated.text = creationDateFormat.format(creationCalendar.time)

            // TODO: Work on language displayed
            val niceLastEdited =
                DateUtils.getRelativeTimeSpanString(listWithItems.shoppingList.timestampOfLastChange)
            tvLastEdited.text = niceLastEdited

            mcvBackground.setOnClickListener {
                onItemClicked?.let {
                    it(listWithItems)
                }
            }

            ibArchive.setOnClickListener {
                onArchiveBtnClicked?.let {
                    it(listWithItems)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}