package com.elynis.app.shoppinglist.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shopping_list")
class ShoppingList(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    var name: String = "",
    var timestampOfCreation: Long = 0L,
    var timestampOfLastChange: Long = 0L,
    var isArchived: Boolean = false
)