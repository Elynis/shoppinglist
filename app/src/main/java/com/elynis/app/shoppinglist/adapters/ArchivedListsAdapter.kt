package com.elynis.app.shoppinglist.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems
import com.elynis.app.shoppinglist.util.divideToPercent
import kotlinx.android.synthetic.main.circular_progress_indicator.view.*
import kotlinx.android.synthetic.main.item_archived_shopping_list.view.*
import java.text.SimpleDateFormat
import java.util.*


class ArchivedListsAdapter : RecyclerView.Adapter<ArchivedListsAdapter.ArchiveListsViewHolder>() {

    private var onItemClicked: ((ShoppingListWithShoppingItems) -> Unit)? = null
    fun setOnItemClicked(listener: (ShoppingListWithShoppingItems) -> Unit) {
        onItemClicked = listener
    }

    private var onUnarchiveBtnClicked: ((ShoppingListWithShoppingItems) -> Unit)? = null
    fun setOnUnarchiveBtnClicked(listener: (ShoppingListWithShoppingItems) -> Unit) {
        onUnarchiveBtnClicked = listener
    }

    inner class ArchiveListsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val diffCallback = object : DiffUtil.ItemCallback<ShoppingListWithShoppingItems>() {
        override fun areItemsTheSame(
            oldItem: ShoppingListWithShoppingItems,
            newItem: ShoppingListWithShoppingItems
        ): Boolean {
            return oldItem.shoppingList.id == newItem.shoppingList.id
        }

        override fun areContentsTheSame(
            oldItem: ShoppingListWithShoppingItems,
            newItem: ShoppingListWithShoppingItems
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(list: List<ShoppingListWithShoppingItems>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArchiveListsViewHolder {
        return ArchiveListsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_archived_shopping_list,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ArchiveListsViewHolder, position: Int) {
        val listWithItems = differ.currentList[position]

        holder.itemView.apply {
            val completedItemsCount = listWithItems.shoppingItems.filter { it.isCompleted }.count()
            val itemsCount = listWithItems.shoppingItems.count()
            val progress = completedItemsCount.divideToPercent(itemsCount)

            "$completedItemsCount / $itemsCount".also { tvProgress.text = it }

            cpiProgress.progress = progress
            tvListName.text = listWithItems.shoppingList.name

            val dateFormat = SimpleDateFormat("dd.MM.yy", Locale.getDefault())

            val creationCalendar = Calendar.getInstance().apply {
                timeInMillis = listWithItems.shoppingList.timestampOfCreation
            }

            tvListCreated.text = dateFormat.format(creationCalendar.time)

            val archivedCalendar = Calendar.getInstance().apply {
                timeInMillis = listWithItems.shoppingList.timestampOfCreation
            }
            val archivedDateString = "Archived: ${dateFormat.format(archivedCalendar.time)}"
            tvArchived.text = archivedDateString

            // TODO: Nieedytowalne
            mcvBackground.setOnClickListener {
                onItemClicked?.let {
                    it(listWithItems)
                }
            }

            ibUnarchive.setOnClickListener {
                onUnarchiveBtnClicked?.let {
                    it(listWithItems)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}