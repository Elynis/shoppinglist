package com.elynis.app.shoppinglist.data.db

import android.content.Context
import com.elynis.app.shoppinglist.R

enum class ShoppingItemUnit(val description: Int, val shouldBeDisplayed : Boolean = true) {
    DEFAULT(
        R.string.unit_description_default
    ),
    GRAMS(
        R.string.unit_description_grams
    ),
    BOTTLES(
        R.string.unit_description_bottles
    ),
    KILOGRAMS(
        R.string.unit_description_kilograms
    ),
    ML(
        R.string.unit_description_mililitres
    ),
    INVALID(0, false);

    companion object {
        fun descriptionOf(context: Context, description: String): ShoppingItemUnit {
            ShoppingItemUnit.values().forEach {
                if (context.getString(it.description) == description) {
                    return it
                }
            }
            return INVALID
        }
    }
}