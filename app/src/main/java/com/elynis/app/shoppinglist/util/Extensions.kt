package com.elynis.app.shoppinglist.util

import kotlin.math.roundToInt

fun Int.divideToPercent(divideTo: Int): Int {
    return if (divideTo == 0) 0
    else (100.0f * (this.toFloat() / divideTo.toFloat())).toInt()
}

fun Double.roundToDecimals(decimals: Int): Double {
    var dotAt = 1
    repeat(decimals) { dotAt *= 10 }
    val roundedValue = (this * dotAt).roundToInt()
    return (roundedValue / dotAt) + (roundedValue % dotAt).toDouble() / dotAt
}

fun Float.roundToDecimals(decimals: Int): Float {
    var dotAt = 1
    repeat(decimals) { dotAt *= 10 }
    val roundedValue = (this * dotAt).roundToInt()
    return (roundedValue / dotAt) + (roundedValue % dotAt).toFloat() / dotAt
}