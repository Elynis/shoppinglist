package com.elynis.app.shoppinglist.data.repositories

import androidx.lifecycle.LiveData
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.data.db.entities.ShoppingList
import com.elynis.app.shoppinglist.data.db.relations.ShoppingListWithShoppingItems

interface ShoppingRepository {
    suspend fun dropShoppingListsTable()
    suspend fun dropShoppingItemsTable()

    suspend fun insertShoppingItem(item: ShoppingItem)
    suspend fun deleteShoppingItem(item: ShoppingItem)

    suspend fun insertShoppingList(list: ShoppingList) : Long
    suspend fun deleteShoppingList(list: ShoppingList)

    fun getAllShoppingItems(): LiveData<List<ShoppingItem>>
    fun getAllShoppingItemsSortedByName(): LiveData<List<ShoppingItem>>
    fun getShoppingItemsOfShoppingList(id: Int) : LiveData<ShoppingListWithShoppingItems>

    fun getAllShoppingLists(): LiveData<List<ShoppingListWithShoppingItems>>
    fun getAllShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>
    fun getAllActiveShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>
    fun getAllArchivedShoppingListsSortedByDate(): LiveData<List<ShoppingListWithShoppingItems>>
}