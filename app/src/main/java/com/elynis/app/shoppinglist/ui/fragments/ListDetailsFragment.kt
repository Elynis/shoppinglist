package com.elynis.app.shoppinglist.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.adapters.ItemsAdapter
import com.elynis.app.shoppinglist.data.db.entities.ShoppingItem
import com.elynis.app.shoppinglist.ui.dialog.AddOrEditDialogListener
import com.elynis.app.shoppinglist.ui.dialog.AddOrEditShoppingItemDialog
import com.elynis.app.shoppinglist.ui.viewmodels.ListDetailsViewModel
import com.elynis.app.shoppinglist.util.Constants
import com.elynis.app.shoppinglist.util.Currency
import com.elynis.app.shoppinglist.util.roundToDecimals
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_list_details.*
import javax.inject.Inject

@AndroidEntryPoint
class ListDetailsFragment : Fragment(R.layout.fragment_list_details) {

    @Inject
    lateinit var itemsAdapter: ItemsAdapter

    private val viewModel: ListDetailsViewModel by viewModels()
    private var addOrEditShoppingItemDialog: AddOrEditShoppingItemDialog? = null

    override fun onPause() {
        addOrEditShoppingItemDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
        super.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLiveData()
        setupRecyclerView()
        subscribeToObservers()

        btnAdd.setOnClickListener {
            addOrEditShoppingItemDialog =
                AddOrEditShoppingItemDialog(requireContext(), object : AddOrEditDialogListener {
                    override fun onAddBtnClicked(item: ShoppingItem) {
                        item.shoppingListId =
                            viewModel.shoppingListWithShoppingItems.value?.shoppingList?.id
                        viewModel.insertShoppingItem(item)
                        addOrEditShoppingItemDialog = null
                    }
                }).apply {
                    show()
                }
        }
    }

    private val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, LEFT or RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = true

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.layoutPosition
            val item = itemsAdapter.differ.currentList[position]
            viewModel.deleteShoppingItem(item)
            Snackbar.make(requireView(),  getString(R.string.snackbar_item_sucessfully_deleted), Snackbar.LENGTH_LONG).apply {
                setAction(getString(R.string.undo)) {
                    viewModel.insertShoppingItem(item)
                }
                show()
            }

        }
    }

    private fun setupLiveData() {
        viewModel.setupLiveDataOfShoppingList(arguments?.getString(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME)?.toIntOrNull())
    }

    private fun subscribeToObservers() {
        viewModel.shoppingListWithShoppingItems.observe(viewLifecycleOwner) {
            it?.shoppingList?.id?.let { id ->
                arguments?.putString(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME, id.toString())
            }

            itemsAdapter.submitList(it.shoppingItems)
            etListName.setText(it.shoppingList.name)

            val productCount = it.shoppingItems.count()
            val productCountString =
                if (productCount > 1 && productCount == 0) "$productCount Products" else "$productCount Product"
            tvProductCount.text = productCountString

            val priceTotal = it.shoppingItems.sumOf { it.cost.toDouble() }.roundToDecimals(2)
            val priceTotalString = "Total: $priceTotal ${Currency.DOLAR.description}"
            tvTotal.text = priceTotalString
        }
    }

    private fun setupRecyclerView() = rvItems.apply {
        itemsAdapter.apply {
            setOnCheckedBtnClicked { position, item ->
                item.isCompleted = !item.isCompleted
                viewModel.insertShoppingItem(item)
            }
            setOnEditBtnClicked {
                addOrEditShoppingItemDialog =
                    AddOrEditShoppingItemDialog(requireContext(), object : AddOrEditDialogListener {
                        override fun onAddBtnClicked(item: ShoppingItem) {
                            item.shoppingListId =
                                viewModel.shoppingListWithShoppingItems.value?.shoppingList?.id
                            viewModel.insertShoppingItem(item)
                            addOrEditShoppingItemDialog = null
                        }
                    }, it).apply {
                        show()
                    }
            }
        }
        adapter = itemsAdapter
        layoutManager = LinearLayoutManager(requireContext())
        ItemTouchHelper(itemTouchCallback).attachToRecyclerView(this)
    }

}