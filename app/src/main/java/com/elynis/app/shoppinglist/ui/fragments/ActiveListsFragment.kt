package com.elynis.app.shoppinglist.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elynis.app.shoppinglist.R
import com.elynis.app.shoppinglist.adapters.ActiveListsAdapter
import com.elynis.app.shoppinglist.ui.viewmodels.ActiveListsViewModel
import com.elynis.app.shoppinglist.util.Constants
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.active_lists_search_view.*
import kotlinx.android.synthetic.main.fragment_active_lists.*

@AndroidEntryPoint
class ActiveListsFragment: Fragment(R.layout.fragment_active_lists) {

    private val viewModel: ActiveListsViewModel by viewModels()

    private lateinit var activeListsAdapter: ActiveListsAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

        viewModel.initCurrentList()
        viewModel.currentLists.observe(viewLifecycleOwner, {
            activeListsAdapter.submitList(it)
        })

        svSearch.apply {
            setQuery("", false)
            setOnQueryTextListener(
            object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let { viewModel.searchShoppingList(it) }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let { viewModel.searchShoppingList(it) }
                    return true
                }

            })
        }
        fab.setOnClickListener {
            navigateToListDetails(null)
        }
    }

    private fun navigateToListDetails(shoppingListId: String?) {
        val bundle = bundleOf(Constants.SHOPPING_LIST_ID_ARGUMENT_NAME to shoppingListId)
        findNavController().navigate(R.id.action_activeListFragment_to_listDetailsFragment, bundle)
    }

    private fun setupRecyclerView() = rvActiveLists.apply{
        activeListsAdapter = ActiveListsAdapter().apply {
            setOnItemClicked{
                navigateToListDetails(it.shoppingList.id.toString())
            }
            setOnArchiveBtnClicked {
                it.shoppingList.isArchived = true
                viewModel.insertShoppingList(it.shoppingList)
            }
        }

        adapter = activeListsAdapter
        layoutManager = LinearLayoutManager(requireContext())
        ItemTouchHelper(itemTouchCallback).attachToRecyclerView(this)
    }

    private val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean = true

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val position = viewHolder.layoutPosition
            val list = activeListsAdapter.differ.currentList[position].shoppingList
            viewModel.deleteShoppingList(list)

            Snackbar.make(requireView(), getString(R.string.snackbar_item_sucessfully_deleted), Snackbar.LENGTH_LONG).apply {
                setAction(getString(R.string.undo)) {
                    viewModel.insertShoppingList(list)
                }
                show()
            }
        }
    }
}

