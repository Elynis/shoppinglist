package com.elynis.app.shoppinglist.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.elynis.app.shoppinglist.data.db.ShoppingItemUnit


@Entity(tableName = "shopping_items")
data class ShoppingItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    var name: String = "",
    var quantity: Int = 0,
    var cost: Float = 0.0f,
    var unit: ShoppingItemUnit = ShoppingItemUnit.DEFAULT,
    var shoppingListId: Int? = null,
    var isCompleted: Boolean = false // Added in Migration 1_2
)
