package com.elynis.app.shoppinglist.ui.viewmodels

import com.elynis.app.shoppinglist.data.repositories.FakeShoppingRepositoryImpl
import org.junit.Assert.*
import org.junit.Before

class ActiveListsViewModelTest{

    private lateinit var viewModel: ActiveListsViewModel

    @Before
    fun setup() {
        viewModel = ActiveListsViewModel(FakeShoppingRepositoryImpl())
    }
}